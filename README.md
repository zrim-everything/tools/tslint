# Zrim-Everything - TSLint

## Introduction

Docker image to easily launch tslint to a project.

## Launcher

The launcher is a simple wrapper to launch tslint executable using 
the default configuration and ignore project. It also allow to specify a different working
directory.

## Usage

Options:
- -w/--dir-path : Specify a another working directory
- --verbose : Enable the verbose mode of the application. You can also use the environment
variable DEBUG=ze-tslint:*
- -c/--config-file : Override the default configuration file
- -i/--ignore-file : Override the default ignore file
- -o/--output-file :
  - console standard : Print the result to the console (Default)
  - json FILE_PATH : Save the report as json to the given file
  
# Docker

The application is only available via the docker image.
By default the working directory is /mnt/usr-project.

Example of a docker file for a project.

```yaml
version: '3.3'
services:
  linter:
    image: registry.gitlab.com/zrim-everything/tools/tslint:latest
    volumes:
      - type: bind
        source: ./
        target: /mnt/usr-project
        read_only: true
networks:
  default:
    external:
      name: none
```

Please consider using a fix version for the image, so you sure to always have the expected version.

## Link

- [TSLint](https://palantir.github.io/tslint/)
