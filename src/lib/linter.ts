import * as pathUtils from "path";
import {tsLintReports} from "./formatters/formatter";
import {createLogger, Logger} from './logger';

export namespace linter {

  export interface RunOnResolve {
    rawReport: tsLintReports.RawReport;
    success: boolean;
  }
}

export interface Linter {
  run(): Promise<linter.RunOnResolve>;
}

export namespace simpleLinter {

  export interface ConstructorOptions {
    nodeBin?: string;
    tslintBin?: string;
    workingDirectory?: string;
    configFilePath: string;
    ignoreFilePath?: string | undefined;
  }

  export interface Properties {
    nodeBin: string;
    tslintBin: string;
    workingDirectory: string;
    configFilePath: string;
    ignoreFilePath: string | undefined;
  }

  export interface RunTsLintCliOptions {
    excludeGlobs: string[];
  }

  export interface RunTsLintCliOnResolve {
    rawReport: tsLintReports.RawReport;
    success: boolean;
  }
}

export class SimpleLinter implements Linter {

  /**
   * Internal properties
   */
  protected properties: simpleLinter.Properties;

  /**
   * The logger
   */
  protected logger: Logger;

  constructor(options: simpleLinter.ConstructorOptions) {
    this.properties = {
      nodeBin: options.nodeBin || 'node',
      tslintBin: options.tslintBin || pathUtils.resolve(__dirname, '../../node_modules/.bin/tslint'),
      workingDirectory: options.workingDirectory || process.cwd(),
      configFilePath: options.configFilePath,
      ignoreFilePath: options.ignoreFilePath
    };

    this.logger = createLogger('linter');
  }

  public async run(): Promise<linter.RunOnResolve> {
    const excludeGlobs = await this.resolveExcludeGlobs();

    const runResponse = this.runTsLintCli({
      excludeGlobs
    });

    return runResponse;
  }

  protected async resolveExcludeGlobs(): Promise<string[]> {
    const fse = require('fs-extra');

    if (this.properties.ignoreFilePath && await fse.pathExists(this.properties.ignoreFilePath)) {
      const data = await fse.readFile(this.properties.ignoreFilePath);
      const lines = data.toString().split('\n').map(item => item.trim()).filter(item => item && item.length > 0);
      return lines;
    }

    return [];
  }

  protected parseNativeTsLinReport(data: string): tsLintReports.RawReport {
    const report = {
      elements: []
    } as tsLintReports.RawReport;

    try {
      const jsonData = JSON.parse(data);
      report.elements = jsonData;
    } catch (error) {
      this.logger.debug(`Failed to parse the json: ${error.message}`);
      throw error;
    }

    return report;
  }

  protected runTsLintCli(options: simpleLinter.RunTsLintCliOptions): Promise<simpleLinter.RunTsLintCliOnResolve> {
    return new Promise((resolve, reject) => {
      const { spawn } = require('child_process');

      const cliArgs = [this.properties.tslintBin];

      // https://palantir.github.io/tslint/usage/cli/
      cliArgs.push('--project', this.properties.workingDirectory);
      cliArgs.push('--config', this.properties.configFilePath);
      cliArgs.push('--format', 'json');
      cliArgs.push('--rules-dir', pathUtils.resolve(`${__dirname}/../../node_modules/tslint-eslint-rules/dist/rules`));

      const {excludeGlobs} = options;
      for (const item of excludeGlobs) {
        cliArgs.push('--exclude', item);
      }

      this.logger.debug(`Run command: node ${cliArgs.join(' ')}`);
      const tsLintProcess = spawn(this.properties.nodeBin, cliArgs);

      let jsonData = '';

      tsLintProcess.stdout.on('data', data => {
        jsonData += data.toString();
      });

      tsLintProcess.stderr.on('data', data => {
        process.stderr.write(data);
      });

      tsLintProcess.on('close', code => {
        this.logger.debug(`Process finished with code ${code}`);
        switch (code) {
          case 0:
          case 2: {
            const rawReport = this.parseNativeTsLinReport(jsonData);
            return resolve({
              rawReport,
              success: code === 0
            });
          }
          case 1:
            return reject(new Error(`Internal error. Invalid command passed to tslint`));
          default:
            return reject(new Error(`Unknown lint result code: ${code}`));
        }
      });
    });
  }
}
