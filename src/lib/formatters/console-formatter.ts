import {Formatter, tsLintReports} from "./formatter";
import * as _ from 'lodash';

export class SimpleConsoleFormatter implements Formatter {

  public async format(nativeReport: tsLintReports.RawReport): Promise<string> {
    const eol = '\n';

    let errorCounter = 0, otherCounter = 0;

    const groups = _.groupBy(nativeReport.elements, 'name');

    let reportStr = '';
    const reportFileLines = [];

    _.each(groups, (items: tsLintReports.ReportItem[], fileName: string) => {
      let reportFile = `${fileName}${eol}`;

      items.sort((a, b) => a.startPosition.line - b.startPosition.line);

      items.forEach(item => {
        reportFile += `${_.padStart(String(item.startPosition.line + 1), 5, ' ')}:${_.padEnd(String(item.startPosition.character + 1), 5, ' ')}`;
        reportFile += ` ${_.padStart(String(item.endPosition.line + 1), 5, ' ')}:${_.padEnd(String(item.endPosition.character + 1), 5, ' ')}`;
        reportFile += ` ${_.padEnd(item.ruleSeverity.toLowerCase(), 10, ' ')}`;
        reportFile += ` ${_.padEnd(item.ruleName, 35, ' ')}`;
        reportFile += ` ${item.failure}`;
        reportFile += `${eol}`;

        switch (item.ruleSeverity.toLowerCase()) {
          case 'error':
            errorCounter++;
            break;
          default:
            otherCounter++;
            break;
        }
      });

      reportFileLines.push(reportFile);
    });

    reportStr += `${reportFileLines.join(eol)}${eol}`;
    reportStr += errorCounter > 0 ? '⤫' : ' ✓';
    reportStr += ` ${errorCounter + otherCounter} problems (${errorCounter} errors, ${otherCounter} others)${eol}`;

    return reportStr;
  }
}
