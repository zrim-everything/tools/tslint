import {Formatter, tsLintReports} from "./formatter";

export class SimpleJsonFormatter implements Formatter {

  public async format(nativeReport: tsLintReports.RawReport): Promise<string> {
    const eol = '\n';

    return `${JSON.stringify(nativeReport.elements)}${eol}`;
  }
}
