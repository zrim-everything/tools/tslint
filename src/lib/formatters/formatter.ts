
export namespace tsLintReports {

  export interface Position {
    character: number;
    line: number;
    position: number;
  }

  export interface Fix {
    innerStart: number;
    innerLength: number;
    innerText: string;
  }

  export interface ReportItem {
    endPosition: Position;
    failure: string;
    fix: Fix[];
    name: string;
    ruleName: string;
    ruleSeverity: string;
    startPosition: Position;
  }

  export interface RawReport {
    elements: ReportItem[];
  }
}

export interface Formatter {
  format(nativeReport: tsLintReports.RawReport): Promise<string>;
}
