import * as createDebug from 'debug';

export interface Logger {
  debug(...args: any[]);
}

export function createLogger(name): Logger {
  return {
    debug: createDebug(`ze-tslint:${name}`)
  };
}
