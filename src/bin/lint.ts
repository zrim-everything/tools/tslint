#!/usr/bin/env node
import {ArgumentParser} from 'argparse';
import * as _ from 'lodash';
import * as pathUtils from 'path';
import {createLogger} from '../lib/logger';
import {SimpleLinter, linter} from '../lib/linter';
import {Formatter} from '../lib/formatters/formatter';
import {SimpleConsoleFormatter} from '../lib/formatters/console-formatter';
import {SimpleJsonFormatter} from '../lib/formatters/json-formatter';
import * as fse from 'fs-extra';

interface AvailableFormatters {
  console: Formatter;
  json: Formatter;
}

interface WorkflowContext {
  nodeBin: string;
  tslintBin: string;
  workingDirectory: string;
  configFilePath: string;
  ignoreFilePath: string;
  formatters: AvailableFormatters;
}

interface HandleSaveReportOptions {
  runResponse: linter.RunOnResolve;
  filePath: string;
}

process.exitCode = 1; // Force error if not other exit code specified

const VERSION = require('./../../package.json').version;

const parser = new ArgumentParser({
  version: VERSION,
  addHelp: true,
  description: 'TSLint launcher'
});

parser.addArgument(['-w', '--dir-path'], {
  help: 'Override the working directory for tslint',
  metavar: 'path',
  dest: 'workingDirectory',
  action: 'store',
  defaultValue: process.cwd()
});

parser.addArgument(['--node-path'], {
  help: 'Node.js binary path',
  metavar: 'path',
  dest: 'nodeBin',
  action: 'store',
  defaultValue: 'node'
});

parser.addArgument(['--verbose'], {
  help: 'Enable verbose mode',
  dest: 'verboseModeEnabled',
  action: 'storeTrue',
  defaultValue: process.env.ZE_ENABLE_VERBOSE_MODE === 'true'
});

parser.addArgument(['-c', '--config-file'], {
  help: 'Override the tslint configuration file',
  metavar: 'path',
  dest: 'tslintConfigFilePath',
  action: 'store',
  defaultValue: '/etc/tslint/default/tslint.yml'
});

parser.addArgument(['-i', '--ignore-file'], {
  help: 'Override the tslint ignore file',
  metavar: 'path',
  dest: 'tslintIgnoreFilePath',
  action: 'store',
  defaultValue: '/etc/tslint/default/.tslintignore'
});


parser.addArgument(['-o', '--output-file'], {
  help: 'Specify an output file',
  dest: 'tslintOutputs',
  action: 'append',
  nargs: 2,
  // @ts-ignore
  metavar: ['format', 'filePath']
});

const userArguments = parser.parseArgs();

if (userArguments.verboseModeEnabled === true) {
  process.env.DEBUG = `${process.env.DEBUG},ze-tslint:*`;
}

const LOGGER = createLogger('launcher');


const workflowContext = {
  nodeBin: userArguments.nodeBin,
  tslintBin: pathUtils.resolve(__dirname, '../../node_modules/.bin/tslint'),
  workingDirectory: userArguments.workingDirectory
} as WorkflowContext;

async function initializeFormatters(): Promise<void> {
  workflowContext.formatters = {
    console: new SimpleConsoleFormatter(),
    json: new SimpleJsonFormatter()
  };
}

async function saveReports(runResponse: linter.RunOnResolve): Promise<void> {
  const userOutputs = userArguments.tslintOutputs || [['console', '']];

  for (const userOutput of userOutputs) {
    switch (userOutput[0]) {
      case 'console':
        await handleConsoleReport({runResponse, filePath: userOutput[1]});
        break;
      case 'json':
        await handleJsonReport({runResponse, filePath: userOutput[1]});
        break;
      default:
        throw new Error(`Unknown format '${userOutput[0]}'`);
    }
  }
}

async function handleConsoleReport(options: HandleSaveReportOptions): Promise<void> {
  const data = await workflowContext.formatters.console.format(options.runResponse.rawReport);
  process.stdout.write(data);
}

async function handleJsonReport(options: HandleSaveReportOptions): Promise<void> {
  const data = await workflowContext.formatters.json.format(options.runResponse.rawReport);

  LOGGER.debug(`Write json data to file '${options.filePath}'`);
  await fse.outputFile(options.filePath, data);
  LOGGER.debug(`Write json data to file '${options.filePath}' succeed`);
}

function initializeLintConfigPaths() {
  const rootProjectPath = pathUtils.resolve(__dirname, '../../');
  workflowContext.configFilePath = pathUtils.resolve(rootProjectPath, userArguments.tslintConfigFilePath);
  LOGGER.debug(`Using the tslint configuration file '${workflowContext.configFilePath}'`);

  workflowContext.ignoreFilePath = pathUtils.resolve(rootProjectPath, userArguments.tslintIgnoreFilePath);
  LOGGER.debug(`Using the tslint ignore file '${workflowContext.ignoreFilePath}'`);
}

async function main() {
  initializeLintConfigPaths();
  await initializeFormatters();

  const linter = new SimpleLinter({
    nodeBin: userArguments.nodeBin,
    workingDirectory: userArguments.workingDirectory,
    configFilePath: workflowContext.configFilePath,
    ignoreFilePath: workflowContext.ignoreFilePath
  });

  const runResponse = await linter.run();

  await saveReports(runResponse);

  if (runResponse.success) {
    process.exitCode = 0;
  }
}


main()
  .then(() => {
    LOGGER.debug("End of the process");
  })
  .catch(error => {
    LOGGER.debug(`Error: ${error.message}\n${error.stack}\n`);
    process.exit(_.isNumber(error.exitCode) ? error.exitCode : 1);
  });


