FROM node:10.15.3-alpine

LABEL maintainer="NEGRO, Eric <yogo95@zrim-everything.eu>" \
    eu.zrim-everything.project.name="tslint" \
    eu.zrim-everything.project.type="tool" \
    eu.zrim-everything.project.description="High level application to run tslint" \
    eu.zrim-everything.project.git.url="https://gitlab.com/zrim-everything/tools/tslint.git" \
    eu.zrim-everything.tools.type="linter"

ENV ZE_LAUNCHER_ROOT_PATH=/usr/local/zrim-everything/tslint-lancher \
    NODE_ENV=production \
    ZE_PROJECT_DIR_PATH=/mnt/usr-project

RUN rm -rf /var/cache/apk/* && \
    rm -rf /tmp/* && \
    apk update && \
    apk upgrade && \
    apk --update --update-cache add --no-cache \
    bash \
    #
    # Clean
    #
    && rm -f /var/cache/apk/* \
    #
    # npm
    #
    && npm install npm@latest -g --unsafe-perm=true \
    #
    # TypeScript
    #
    && npm install typescript@3.3 -g --unsafe-perm=true \
    #
    # Application directory creation
    #
    && mkdir -p $ZE_LAUNCHER_ROOT_PATH \
    && mkdir -p /etc/tslint/default

COPY tslint.yml .tslintignore /etc/tslint/default/
COPY package.json package-lock.json tsconfig.json $ZE_LAUNCHER_ROOT_PATH/
COPY src/ $ZE_LAUNCHER_ROOT_PATH/src/

# Build the packages
RUN npm set progress=true \
    && npm config set depth 0 \
    && cd $ZE_LAUNCHER_ROOT_PATH \
    # Do not know why npm install do not install dev dependencies
    && npm install --only=prod \
    && npm install --only=dev \
    && npm cache clean --force \
    && tsc \
    && tsc --declaration \
    # Clean module to re-install them later in prod mod
    && rm -Rf node_modules src

RUN npm set progress=false \
    && npm config set depth 0 \
    && cd $ZE_LAUNCHER_ROOT_PATH \
    && npm install --only=production \
    && npm cache clean --force \
    && chmod a+x $ZE_LAUNCHER_ROOT_PATH/dist/bin/*.js \
    && ln -s $ZE_LAUNCHER_ROOT_PATH/dist/bin/lint.js /usr/bin/lint \
    && ln -s $ZE_LAUNCHER_ROOT_PATH/dist/bin/lint.js /usr/bin/ze-tslint \
    && ln -s $ZE_LAUNCHER_ROOT_PATH/node_modules/.bin/tslint /usr/bin/tslint

# Set working directory
WORKDIR $ZE_PROJECT_DIR_PATH
CMD ["lint"]

# internal version
ARG VERSION
RUN cd $ZE_LAUNCHER_ROOT_PATH \
    && npm version --allow-same-version --no-git-tag-version $VERSION
